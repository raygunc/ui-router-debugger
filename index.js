/*
 *    Name: ui-router.debugger.js
 *    Description: Used to help debug ui-router Angular library by outputting state and change info.
 *    Usage:  angular.module('MyModule', ['ConsoleLogger'])
 *                .run(['PrintToConsole', function(PrintToConsole) {
 *                    PrintToConsole.active = true;
 *                }]);
 *    Source: http://stackoverflow.com/posts/26086754/revisions
 */
'use strict';

require('angular');
require('./src/ui-router-debugger');

//Export namespaces
module.exports = 'UiRouterDebugger';