## UI Router Debugger

### Description
Used to help debug ui-router Angular library by outputting state and change info.      


### Usage

Use the following code in your module's `.run()` block to enable debugging. 

```
angular.module('MyModule', ['UiRouterDebugger'])
   .run(['PrintToConsole', function(PrintToConsole) {
        PrintToConsole.active = true;
   }]);
```

### Note
This was adapted from the following Stackoverflow post referenced below. I take no credit for this, I just wanted to make it handy NPM package for easy use with Angular. 

#### Original source
http://stackoverflow.com/posts/26086754/revisions